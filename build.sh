#!/bin/sh

#mkdir -p build && cd build && cmake .. -DCMAKE_BUILD_TYPE:STRING=Debug && make -j 8 && cd .. && ls -l bin && ls -l lib
mkdir -p build && cd build && cmake .. -DCMAKE_BUILD_TYPE:STRING=Debug && make && cd .. && ls -l bin && ls -l lib
chmod -R 0755 bin
mkdir -p debian/usr/bin
cp bin/ns1 debian/usr/bin
sudo dpkg-deb --build debian
mv -f debian.deb bin/ns1.deb
