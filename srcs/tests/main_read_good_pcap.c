#include "utils.h"
#include <unistd.h>
#include <stdio.h>

int	main()
{
  unit_tests_read_pcap("srcs/tests/res/test_unit_good.pcap");

  printf("\n\n\\!/                                  \\!/\n");
  printf("Next test with dump in 5 seconds ...\n");
  sleep(5);
  set_dump_enable(1);
  unit_tests_read_pcap("srcs/tests/res/test_unit_good.pcap");
  return 1;
}
