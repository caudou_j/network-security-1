#include <stdio.h>
#include "../core/forge.h"
#include "utils.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../core/packet.h"
#include "../generic-list/generic_list.h"
#include "../core/layers/layer_network_ipv4.h"

int	main()
{
  char *packet;
  int len;
  struct sockaddr_in sin;
  const int port_dst = 53;
  const char *dest_ip = "20.21.22.23";

  sin.sin_family = AF_INET;
  sin.sin_port = htons(port_dst);
  sin.sin_addr.s_addr = inet_addr (dest_ip);

  // FORGE PACKET
  len = get_dns_forged_packet("192.168.0.122", // ip src
			      1025, // port src
			      port_dst, // port dst
			      "www.google.com", // hostname
			      255, // ttl
			      &packet,
			      &sin);

  if (len != 60)
    {
      fail("Invalid packet size");
      return -1;
    }
  success("packet size is correct");

  // WRITE to pcap
  // todo

  // READ from pcap
  set_dump_enable(1);
  unit_tests_read_pcap("srcs/tests/res/test_unit_good.pcap");
  return 1;
}
