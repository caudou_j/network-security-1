#include <stdio.h>
#include "../core/forge.h"
#include "utils.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../core/packet.h"
#include "../generic-list/generic_list.h"
#include "../core/layers/layer_network_ipv4.h"

static t_bool find_node(void *value, void *param)
{
  if ((unsigned long) ((layer_node *) value)->type == (unsigned long) param)
    return glTRUE;
  return glFALSE;
}

int	main()
{
  char	buffer[4096];
  char *packet;
  int len;
  struct sockaddr_in sin;
  const int port_dst = 53;
  const char *dest_ip = "20.21.22.23";
  parse_params *params;
  t_list	packet_data;

  sin.sin_family = AF_INET;
  sin.sin_port = htons(port_dst);
  sin.sin_addr.s_addr = inet_addr (dest_ip);

  // FORGE PACKET
  len = get_dns_forged_packet("192.168.0.122", // ip src
			      1025, // port src
			      port_dst, // port dst
			      "www.google.com", // hostname
			      255, // ttl
			      &packet,
			      &sin);

  if (len != 60)
    {
      fail("Invalid packet size");
      return -1;
    }
  success("packet size is correct");

  // PARSE PACKET
  packet_data = create_and_fill_params(len, (u_char *)packet);
  parse_layer_network_ipv4(packet_data, (u_char *)packet);
  params = (parse_params *) list_get_elem_like(packet_data,
					       find_node,
					       (void *)NODE_TYPE_PARSE_PARAMS);

  // DUMP
  if (params->parse_error_layer != -1)
    {
      snprintf(buffer, 4096, "Parser layers failed at level %d", params->parse_error_layer);
      return fail(buffer);
    }
  success("Packet succesfully parsed");

  printf("\n---- DUMP packet -----\n");
  list_dump(packet_data, dump_packet_data_node);
  success("Done");
  return (0);
}
