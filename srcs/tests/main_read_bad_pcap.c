#include <stdio.h>
#include "utils.h"

int	main()
{
  printf("Test : read empty file\n");
  unit_tests_read_pcap("srcs/tests/res/test_unit_bad1.pcap");
  printf("\n");
  printf("Test : read file with cutted packet header\n");
  unit_tests_read_pcap("srcs/tests/res/test_unit_bad2.pcap");
  printf("\n");
  printf("Test : read file with invalid buffer size\n");
  unit_tests_read_pcap("srcs/tests/res/test_unit_bad3.pcap");
  return 1;
}
