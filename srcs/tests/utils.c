#include "utils.h"
#include <stdio.h>
#include "../core/xexit.h"
#include "../core/pcap.h"
#include "../core/packet.h"
#include "../generic-list/generic_list.h"

static int g_enable_dump = 0;

static t_bool find_node(void *value, void *param)
{
  if ((unsigned long) ((layer_node *) value)->type == (unsigned long) param)
    return glTRUE;
  return glFALSE;
}

void set_dump_enable(int is_enable)
{
  g_enable_dump = is_enable;
}

int fail(const char *what)
{
  printf("\e[31m[ Failed ]\e[39m %s\n", what);
  return -1;
}

int success(const char *what)
{
  printf("\e[32m[ Success ]\e[39m %s\n", what);
  return 0;
}

int	unit_tests_read_pcap(const char *path)
{
  char	buffer[4096];
  t_list	packet_data;
  FILE		*fd;
  struct pcap_file_header gheader;
  struct pcap_sf_pkthdr pheader;
  int ret;
  parse_params *params;
  int count;

  printf("---- UNIT TESTS on %s ----\n", path);
  if (!(fd = fopen(path, "r")))
    return fail("Open pcap file");
  success("Open pcap file");
  if (pcap_read_header(fd, &gheader) == 0)
    return fail("Read DNS global header");
  success("Read DNS global header");

  count = 0;
  while ((ret = pcap_read_packet(fd, &pheader, (u_char *)buffer)) != 0)
    {
      if (ret != 1)
	{
	  fclose(fd);
	  return fail("Read packet header and data");
	}

      packet_data = parse_packet(pheader.caplen, (u_char *)buffer);
      params = (parse_params *) list_get_elem_like(packet_data,
						   find_node,
						   (void *)NODE_TYPE_PARSE_PARAMS);
      if (params->parse_error_layer != -1)
	{
	  snprintf(buffer, 4096, "Parser layers failed at level %d", params->parse_error_layer);
	  fclose(fd);
	  return fail(buffer);
	}
      if (g_enable_dump)
	{
	  printf("\n---- packet -----\n");
	  list_dump(packet_data, dump_packet_data_node);
	}
      list_clear(&packet_data, clear_packet_data_node);
      ++count;
    }
  if (count > 0)
    {
      snprintf(buffer, 4096, "%d packets have been read and parsed succesfully", count);
      success(buffer);
    }
  else
    {
      fail("0 packet were found");
    }
  success("Done !");
  fclose(fd);
  return 0;
}
