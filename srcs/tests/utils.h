#ifndef		UTILS_H_
# define	UTILS_H_

int fail(const char *what);
int success(const char *what);
int	unit_tests_read_pcap(const char *path);
void set_dump_enable(int is_enable);

#endif /* !UTILS_H_ */
