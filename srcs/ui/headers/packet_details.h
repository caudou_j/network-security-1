#ifndef PACKETDETAILS_H_
# define PACKETDETAILS_H_

#include <gtk/gtk.h>
#include "home_window.h"

void	initPacketDetails(t_HomeWindow *);
void    setPacketDetails(t_HomeWindow *);

#endif /* !PACKETDETAILS_H_ */
