#ifndef FILTERS_H_
# define FILTERS_H_

#include <gtk/gtk.h>
#include "home_window.h"

void    clearFilters(t_HomeWindow *); 
void	filtersContent(t_HomeWindow *);

#endif /* !FILTERS_H_ */
