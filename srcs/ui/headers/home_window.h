#ifndef HOMEWINDOW_H_
# define HOMEWINDOW_H_

#include <gtk/gtk.h>

typedef struct		s_PacketFilter
{
  const gchar                 *ip_src;
  const gchar                 *ip_dst;
  const gchar                 *port_src;
  const gchar                 *port_dst;
  gchar                 *application;
  gchar                 *protocol;
  gchar                 *details;
} t_PacketFilter;

typedef struct		s_PacketInfoss
{
  gchar                 *mac_src;
  gchar                 *mac_dst;
  gchar                 *ip_src;
  gchar                 *ip_dst;
  gchar                 *port_src;
  gchar                 *port_dst;
  gchar                 *application;
  gchar                 *protocol;
  gchar                 *details;
} t_PacketInfoss;

typedef struct		s_PacketInfos
{
  gchar                 mac_src[20];
  gchar                 mac_dst[20];
  gchar                 ip_src[20];
  gchar                 ip_dst[20];
  gchar                 port_src[20];
  gchar                 port_dst[20];
  gchar                 application[20];
  gchar                 protocol[20];
  gchar                 details[4096];
} t_PacketInfos;

typedef struct		s_HomeWindow
{
  GtkApplication *app;
  GtkWidget      *grid;
  GtkWidget      *window;
  GtkWidget      *s_window;
  GtkWidget      *f_window;
  t_PacketInfos  *packet_details;
  t_PacketInfoss  *packet_detailss;
  t_PacketInfos  *new_packet;
  int            refresh;
  GtkWidget      *treeView;
  GtkTreeModel   *model;
  GtkListStore   *store;
  GtkTreeIter    iter;
  GtkEntry	*pcap_edit_text;
  GtkWidget      *details_labels[25];
  GtkWidget      *forge_labels[25];
  GtkWidget      *forge_entries[25];
  GtkWidget      *forge_drop[25];
  t_PacketFilter  *filters;
} t_HomeWindow;

void clearWindow(t_HomeWindow *);
void	initWindow(t_HomeWindow *);
void	content(t_HomeWindow *);


#endif /* !HOMEWINDOW_H_ */
