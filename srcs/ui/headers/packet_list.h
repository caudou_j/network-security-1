#ifndef PACKET_LIST_H_
# define PACKET_LIST_H_

#include <gtk/gtk.h>
#include "home_window.h"

GtkWidget *create_view_and_model(t_HomeWindow *);
GtkTreeModel *create_and_fill_model(t_HomeWindow *);

#endif /* !PACKET_LIST_H_ */
