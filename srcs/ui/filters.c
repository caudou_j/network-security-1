#include "filters.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void filtersPacket (GtkWidget *widget, t_HomeWindow *window) {
  const gchar *entry1 = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[0]));
  window->filters->ip_src = entry1;
  const gchar *entry2 = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[1]));
  window->filters->ip_dst = entry2;
  const gchar *entry3 = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[2]));
  window->filters->port_src = entry3;
  const gchar *entry4 = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[3]));
  window->filters->port_dst = entry4;

  gchar *drop1 = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(window->forge_drop[0]));
  window->filters->protocol = drop1;


  (void) widget;
}

void clearFilters (t_HomeWindow *window) {
  gtk_combo_box_set_active(GTK_COMBO_BOX(window->forge_drop[0]), -1);
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[0]), "");
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[1]), "");
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[2]), "");
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[3]), "");
}

void filtersContent (t_HomeWindow *window) {
  GtkWidget *button;

  window->filters = malloc(sizeof(t_PacketFilter));
  bzero(window->filters, sizeof(t_PacketFilter));

   /* Create Label */
  window->forge_labels[0] = gtk_label_new ("Filters");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[0], 5, 3, 2, 1);
  /* Create Label */
  window->forge_labels[0] = gtk_label_new ("Protocol");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[0], 5, 4, 2, 1);
  /* Set Combo dropdown */
  window->forge_drop[0] = gtk_combo_box_text_new();
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(window->forge_drop[0]), NULL, "UDP");
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(window->forge_drop[0]), NULL, "TCP");
  gtk_grid_attach(GTK_GRID (window->grid),  window->forge_drop[0], 5, 5, 2, 1);

  window->forge_labels[1] = gtk_label_new ("Ip address (src):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[1], 5, 6, 2, 1);
  window->forge_entries[0] = gtk_entry_new();
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[0], 5, 7, 2, 1);

  window->forge_labels[2] = gtk_label_new ("Ip address (dst):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[2], 5, 8, 2, 1);
  window->forge_entries[1] = gtk_entry_new();
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[1], 5, 9, 2, 1);

  window->forge_labels[3] = gtk_label_new ("Port (src):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[3], 5, 10, 2, 1);
  window->forge_entries[2] = gtk_entry_new();
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[2], 5, 11, 2, 1);

  window->forge_labels[4] = gtk_label_new ("Port (dst:");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[4], 5, 12, 2, 1);
  window->forge_entries[3] = gtk_entry_new();
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[3], 5, 13, 2, 1);

  button = gtk_button_new_with_label ("Ok");
  g_signal_connect (button, "clicked", G_CALLBACK (filtersPacket), window);
  gtk_grid_attach (GTK_GRID (window->grid), button, 5, 14, 2, 1);

  gtk_widget_show_all(window->window);
}
