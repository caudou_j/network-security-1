#include "../core/xexit.h"
#include "../core/network_capture.h"
#include "../core/layers/layer_data_link_ethernet.h"
#include "../core/layers/layer_network_ipv4.h"
#include "../core/layers/layer_transport_udp.h"
#include "../core/layers/layer_transport_tcp.h"
#include "../core/layers/layer_application_http.h"
#include "../core/layers/layer_application_dns.h"
#include <stdlib.h>
#include "topbar.h"
#include "packet_details.h"
#include "forge.h"
#include "filters.h"
#include "packet_list.h"
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <arpa/inet.h>

static pthread_t g_thread = 0;

static t_bool find_node(void *value, void *param)
{
  if ((unsigned long) ((layer_node *) value)->type == (unsigned long) param)
    return glTRUE;
  return glFALSE;
}

static int filter_packet(t_PacketInfos *packet, t_PacketFilter *filters)
{
  if (filters->ip_src != NULL &&
      strlen(filters->ip_src) > 0 &&
      strcmp(filters->ip_src, packet->ip_src) != 0)
    return 0;
  if (filters->ip_dst != NULL &&
      strlen(filters->ip_dst) > 0 &&
      strcmp(filters->ip_dst, packet->ip_dst) != 0)
    return 0;
  if (filters->port_src != NULL &&
      strlen(filters->port_src) > 0 &&
      strcmp(filters->port_src, packet->port_src) != 0)
    return 0;
  if (filters->port_dst != NULL &&
      strlen(filters->port_dst) > 0 &&
      strcmp(filters->port_dst, packet->port_dst) != 0)
    return 0;
  if (filters->protocol != NULL &&
      strlen(filters->protocol) > 0 &&
      strcmp(filters->protocol, packet->protocol) != 0)
   return 0;
  return 1;
}

static void packet_handler(t_list data, void *arg)
{
  t_PacketInfos *new_p;
  t_HomeWindow *window = (t_HomeWindow *)arg;
  layer_ethernet *leth;
  layer_network_ipv4 *ln_ipv4;
  layer_transport_udp *lt_udp;
  layer_transport_tcp *lt_tcp;
  layer_application_http *la_http;
  layer_application_dns *la_dns;

  if ((new_p = malloc(sizeof(t_PacketInfos))) == NULL)
    xexit();
  bzero(new_p, sizeof(t_PacketInfos));
  leth = (layer_ethernet *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_ETHERNET);
  snprintf(new_p->mac_src, 20, "%02x:%02x:%02x:%02x:%02x:%02x",
	   leth->ethhdr.h_source[0],
	   leth->ethhdr.h_source[1],
	   leth->ethhdr.h_source[2],
	   leth->ethhdr.h_source[3],
	   leth->ethhdr.h_source[4],
	   leth->ethhdr.h_source[5]);
  snprintf(new_p->mac_dst, 20, "%02x:%02x:%02x:%02x:%02x:%02x",
	   leth->ethhdr.h_dest[0],
	   leth->ethhdr.h_dest[1],
	   leth->ethhdr.h_dest[2],
	   leth->ethhdr.h_dest[3],
	   leth->ethhdr.h_dest[4],
	   leth->ethhdr.h_dest[5]);

  ln_ipv4 = (layer_network_ipv4 *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_IPV4);
  if (ln_ipv4 != NULL)
    {
      snprintf(new_p->ip_src, 20, "%s", inet_ntoa(*((struct in_addr*) &(ln_ipv4->iphdr.saddr))));
      snprintf(new_p->ip_dst, 20, "%s", inet_ntoa(*((struct in_addr*) &(ln_ipv4->iphdr.daddr))));
      snprintf(new_p->protocol, 20, "%s", get_transport_layer_name(&(ln_ipv4->iphdr)));

      lt_tcp = (layer_transport_tcp *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_TCP);
      if (lt_tcp != NULL)
	{
	  snprintf(new_p->port_src, 20, "%d", htons(lt_tcp->tcphdr.source));
	  snprintf(new_p->port_dst, 20, "%d", htons(lt_tcp->tcphdr.dest));
	  snprintf(new_p->application, 20, "%s", get_tcp_application_name(lt_tcp));

	  la_http = (layer_application_http *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_HTTP);
	  if (la_http != NULL)
	    {
	      snprintf(new_p->details, 4096, "%s", la_http->buffer);
	    }

	  la_dns = (layer_application_dns *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_DNS);
	  if (la_dns != NULL)
	    {
	      snprintf(new_p->details, 50, "%s", la_dns->data);
	    }
	}
      else
	{
	  lt_udp = (layer_transport_udp *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_UDP);
	  if (lt_udp != NULL)
	    {
	      snprintf(new_p->port_src, 20, "%d", htons(lt_udp->udphdr.source));
	      snprintf(new_p->port_dst, 20, "%d", htons(lt_udp->udphdr.dest));
	      snprintf(new_p->application, 20, "%s", get_udp_application_name(lt_udp));

	      la_dns = (layer_application_dns *) list_get_elem_like(data, find_node, (void *)NODE_TYPE_DNS);
	      if (la_dns != NULL)
		{
		  snprintf(new_p->details, 4096, "%s", la_dns->data);
		}
	    }
	}
    }

  if (filter_packet(new_p, window->filters))
    {
      window->new_packet = new_p;
      window->model = create_and_fill_model (window);
      gtk_tree_view_set_model (GTK_TREE_VIEW (window->treeView), window->model);
    }
}

static void *thread_read_pcap_file(void *arg)
{
  t_HomeWindow *window = (t_HomeWindow *)arg;
  const char *filename;

  filename = gtk_entry_get_text(window->pcap_edit_text);
  if (strlen(filename) == 0)
    filename = "mypcap.pcap";
  start_capture_from_file(packet_handler, arg, filename);
  g_thread = 0;
  pthread_exit(NULL);
}

static void *thread_start_capture_and_export(void *arg)
{
  t_HomeWindow *window = (t_HomeWindow *)arg;
  const char *filename;

  filename = gtk_entry_get_text(window->pcap_edit_text);
  if (strlen(filename) > 0)
    start_capture_to_file(packet_handler, arg, filename);
  g_thread = 0;
  pthread_exit(NULL);
}

static void *thread_start_capture(void *arg) {
  start_capture(packet_handler, arg);
  g_thread = 0;
  pthread_exit(NULL);
}

static void capture(GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;
  clearWindow(window);
  topBar(window, 1);
  content(window);
  initPacketDetails(window);
}

static void forge (GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;
  clearWindow(window);
  topBar(window, 0);
  forgeContent(window);
}

static void filters (GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;
  (void) window;

  clearFilters(window);
}

static void play (GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;

  if (g_thread == 0)
    pthread_create(&g_thread, NULL, thread_start_capture, window);
}

static void play_and_export(GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;

  if (g_thread == 0)
    pthread_create(&g_thread, NULL, thread_start_capture_and_export, window);
}

static void read_pcap_file(GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;

  if (g_thread == 0)
    pthread_create(&g_thread, NULL, thread_read_pcap_file, window);
}

static void stop (GtkWidget *widget, t_HomeWindow *window) {
  (void) widget;
  (void) window;
  /* Fonction qui sers peux etre a rien en faite */

  pthread_cancel(g_thread);
  g_thread = 0;
}

void topBar (t_HomeWindow *window, int type) {
  GtkWidget *button;

  (void) window;
  /* Create button and set callback */
  button = gtk_button_new_with_label ("Capture");
  g_signal_connect (button, "clicked", G_CALLBACK (capture), window);

  /* Place the first button in the grid cell (0, 0), and make it fill
   * just 1 cell horizontally and vertically (ie no spanning)
   */
  gtk_grid_attach (GTK_GRID (window->grid), button, 18, 0, 1, 1);

  button = gtk_button_new_with_label ("Forge");
  g_signal_connect (button, "clicked", G_CALLBACK (forge), window);

  /* Place the second button in the grid cell (1, 0), and make it fill
   * just 1 cell horizontally and vertically (ie no spanning)
   */
  gtk_grid_attach (GTK_GRID (window->grid), button, 19, 0, 1, 1);

  if (type == 1) {
    button = gtk_button_new_with_label ("Start");
    g_signal_connect (button, "clicked", G_CALLBACK (play), window);
    gtk_grid_attach (GTK_GRID (window->grid), button, 0, 1, 1, 1);

    button = gtk_button_new_with_label ("Start & export");
    g_signal_connect (button, "clicked", G_CALLBACK (play_and_export), window);
    gtk_grid_attach (GTK_GRID (window->grid), button, 1, 1, 1, 1);

    button = gtk_button_new_with_label ("Read file");
    g_signal_connect (button, "clicked", G_CALLBACK (read_pcap_file), window);
    gtk_grid_attach (GTK_GRID (window->grid), button, 2, 1, 1, 1);

    window->pcap_edit_text = (GtkEntry  *) gtk_entry_new();
    gtk_grid_attach (GTK_GRID (window->grid), (GtkWidget *) window->pcap_edit_text, 3, 1, 1, 1);

    button = gtk_button_new_with_label ("Stop");
    g_signal_connect (button, "clicked", G_CALLBACK (stop), window);
    gtk_grid_attach (GTK_GRID (window->grid), button, 4, 1, 1, 1);

    button = gtk_button_new_with_label ("Clear Filters");
    g_signal_connect (button, "clicked", G_CALLBACK (filters), window);
    gtk_grid_attach (GTK_GRID (window->grid), button, 9, 3, 1, 1);
  }
    /*
  g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window->window);
  gtk_grid_attach (GTK_GRID (window->grid), button, 0, 1, 2, 1);
    */


  gtk_widget_show_all (window->window);
}
