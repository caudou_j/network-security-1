#include <gtk/gtk.h>
#include "home_window.h"
#include "topbar.h"
#include <stdlib.h>

static void activate(GtkApplication *app, t_HomeWindow *w)
{
  w->app = app;
  w->refresh = 1;
  initWindow(w);
  /* Create and Load Content in grid */
  topBar(w, 1);
  content(w);
  gtk_widget_show_all (w->window);
}

static void init_app(t_HomeWindow *window)
{
  window->app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(window->app, "activate", G_CALLBACK (activate), window);
}

int display_window(int argc, char **argv)
{
  t_HomeWindow *w;
  int status;

  w = malloc(sizeof(t_HomeWindow));
  init_app(w);

  status = g_application_run (G_APPLICATION (w->app), argc, argv);
  g_object_unref (w->app);

  return status;
}
