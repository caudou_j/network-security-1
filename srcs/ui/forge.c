#include "forge.h"
#include "core/forge.h"
#include <stdlib.h>
#include <stdio.h>

void forgePacket (GtkWidget *widget, t_HomeWindow *window) {
  const gchar *ip_src = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[0]));
  const gchar *port_src = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[1]));
  const gchar *ip_dst = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[2]));
  const gchar *port_dst = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[3]));
  const gchar *data = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[4]));
  const gchar *ttl = gtk_entry_get_text(GTK_ENTRY (window->forge_entries[5]));
  int port_src_i, port_dst_i, ttl_i;

  if ((port_dst_i = atoi(port_dst)) <= 0) {
    port_dst_i = 80;
  }
  if ((port_src_i = atoi(port_src)) <= 0) {
    port_src_i = 80;
  }
  if ((ttl_i = atoi(ttl)) <= 0) {
    ttl_i = 300;
  }

  forge(ip_src, port_src_i, ip_dst, port_dst_i, data, ttl_i);
  (void)widget;
}

//GtkWidget* createConsoleBox() {
//  GtkWidget* textArea = gtk_text_view_new();
//  GtkWidget* scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
//  GtkWidget* textEntry = gtk_entry_new();
//  GtkWidget* console = gtk_grid_new();
//
//  gtk_container_add(GTK_CONTAINER(scrolledwindow), textArea);
//  gtk_grid_attach(GTK_GRID(console), scrolledwindow, 0, 1, 0, 1);
//  gtk_grid_attach(GTK_GRID(console), textEntry, 0, 1, 1, 2);
//
//  return console;
//}
//﻿163.5.223.45
void forgeContent (t_HomeWindow *window) {
  GtkWidget *button;

  window->forge_labels[0] = gtk_label_new ("Ip address (src):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[0], 0, 2, 2, 1);
  window->forge_entries[0] = gtk_entry_new();
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[0], 0, 3, 2, 1);

  window->forge_labels[1] = gtk_label_new ("Port (src):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[1], 0, 4, 2, 1);
  window->forge_entries[1] = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[1]), "45274");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[1], 0, 5, 2, 1);

  window->forge_labels[2] = gtk_label_new ("Ip address (dst):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[2], 0, 6, 2, 1);
  window->forge_entries[2] = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[2]), "208.67.222.222");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[2], 0, 7, 2, 1);

  window->forge_labels[3] = gtk_label_new ("Port (dst):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[3], 0, 8, 2, 1);
  window->forge_entries[3] = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[3]), "53");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[3], 0, 9, 2, 1);

  window->forge_labels[4] = gtk_label_new ("Hostname");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[4], 0, 10, 2, 1);
  window->forge_entries[4] = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[4]), "www.google.com");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[4], 0, 11, 2, 1);

  window->forge_labels[5] = gtk_label_new ("TTL");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_labels[5], 0, 12, 2, 1);
  window->forge_entries[5] = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(window->forge_entries[5]), "255");
  gtk_grid_attach (GTK_GRID (window->grid),  window->forge_entries[5], 0, 13, 2, 1);

  button = gtk_button_new_with_label ("Send");
  g_signal_connect (button, "clicked", G_CALLBACK (forgePacket), window);
  gtk_grid_attach (GTK_GRID (window->grid), button, 0, 14, 2, 1);

  gtk_widget_show_all(window->window);
}
