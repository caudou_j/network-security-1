#include "packet_details.h"
#include "filters.h"
#include <gtk/gtk.h>

void initPacketDetails (t_HomeWindow *window) {
  /* init details + attach labels to grid */
  window->details_labels[0] = gtk_label_new ("Ethernet");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[0], 0, 3, 1, 1);

  window->details_labels[1] = gtk_label_new ("Mac address (src):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[1], 0, 4, 1, 1);

  window->details_labels[2] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[2], 1, 4, 4, 1);

  window->details_labels[3] = gtk_label_new ("Mac address (dst):");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[3], 0, 5, 1, 1);

  window->details_labels[4] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[4], 1, 5, 4, 1);

  window->details_labels[5] = gtk_label_new ("Ip");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[5], 0, 6, 1, 1);

  window->details_labels[6] = gtk_label_new ("Ip address (src)");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[6], 0, 7, 1, 1);

  window->details_labels[7] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[7], 1, 7, 4, 1);

  window->details_labels[8] = gtk_label_new ("Ip address (dst)");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[8], 0, 8, 1, 1);

  window->details_labels[9] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[9], 1, 8, 4, 1);

  window->details_labels[10] = gtk_label_new ("Port");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[10], 0, 9, 1, 1);

  window->details_labels[11] = gtk_label_new ("Port (src)");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[11], 0, 10, 1, 1);

  window->details_labels[12] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[12], 1, 10, 4, 1);

  window->details_labels[13] = gtk_label_new ("Port (dst)");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[13], 0, 11, 1, 1);

  window->details_labels[14] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[14], 1, 11, 4, 1);

  window->details_labels[15] = gtk_label_new ("Others");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[15], 0, 12, 1, 1);

  window->details_labels[16] = gtk_label_new ("Applications");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[16], 0, 13, 1, 1);

  window->details_labels[17] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[17], 1, 13, 4, 1);

  window->details_labels[18] = gtk_label_new ("Protocol");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[18], 0, 14, 1, 1);

  window->details_labels[19] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[19], 1, 14, 4, 1);

  window->details_labels[20] = gtk_label_new ("Details :");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[20], 0, 15, 1, 1);

  window->details_labels[21] = gtk_label_new ("");
  gtk_grid_attach (GTK_GRID (window->grid),  window->details_labels[21], 1, 15, 4, 5);

  filtersContent(window);

  gtk_widget_show_all (window->window);
}

void setPacketDetails (t_HomeWindow *window) {
  /* reSet details */
  gtk_label_set_text( GTK_LABEL(window->details_labels[2]), window->packet_detailss->mac_src);
  gtk_label_set_text( GTK_LABEL(window->details_labels[4]), window->packet_detailss->mac_dst);
  gtk_label_set_text( GTK_LABEL(window->details_labels[7]), window->packet_detailss->ip_src);
  gtk_label_set_text( GTK_LABEL(window->details_labels[9]), window->packet_detailss->ip_dst);
  gtk_label_set_text( GTK_LABEL(window->details_labels[12]), window->packet_detailss->port_src);
  gtk_label_set_text( GTK_LABEL(window->details_labels[14]), window->packet_detailss->port_dst);
  gtk_label_set_text( GTK_LABEL(window->details_labels[17]), window->packet_detailss->application);
  gtk_label_set_text( GTK_LABEL(window->details_labels[19]), window->packet_detailss->protocol);
  gtk_label_set_text( GTK_LABEL(window->details_labels[21]), window->packet_detailss->details);
  gtk_widget_show_all (window->window);
}
