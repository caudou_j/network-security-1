#include <stdlib.h>
#include "packet_details.h"
#include "packet_list.h"
#include "../core/xexit.h"

enum
{
  COL_MAC_SRC,
  COL_MAC_DST,
  COL_IP_SRC,
  COL_IP_DST,
  COL_PROTOCOL,
  COL_PORT_SRC,
  COL_PORT_DST,
  COL_APPLICATION,
  COL_DETAIL,
  NUM_COLS
} ;

gboolean view_selection_func (GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path, gboolean path_currently_selected, gpointer userdata) {
  GtkTreeIter   iter;
  t_PacketInfoss *details;
  t_HomeWindow  *window = (t_HomeWindow *)userdata;

  (void) selection;
  (void) path_currently_selected;
  if ((details = malloc(sizeof(t_PacketInfos))) == NULL)
    xexit();
  if (gtk_tree_model_get_iter(model, &iter, path))
    {
      gtk_tree_model_get(model, &iter, COL_MAC_SRC, &(details->mac_src), -1);
      gtk_tree_model_get(model, &iter, COL_MAC_DST, &(details->mac_dst), -1);
      gtk_tree_model_get(model, &iter, COL_IP_SRC, &(details->ip_src), -1);
      gtk_tree_model_get(model, &iter, COL_IP_DST, &(details->ip_dst), -1);
      gtk_tree_model_get(model, &iter, COL_PORT_SRC, &(details->port_src), -1);
      gtk_tree_model_get(model, &iter, COL_PORT_DST, &(details->port_dst), -1);
      gtk_tree_model_get(model, &iter, COL_APPLICATION, &(details->application), -1);
      gtk_tree_model_get(model, &iter, COL_PROTOCOL, &(details->protocol), -1);
      gtk_tree_model_get(model, &iter, COL_DETAIL, &(details->details), -1);
      window->packet_detailss = details;
      setPacketDetails(window);
    }
  return TRUE; /* allow selection state to change */
}

GtkTreeModel *create_and_fill_model (t_HomeWindow *window) {
  gtk_list_store_append (window->store, &(window->iter));
  gtk_list_store_set (window->store, &(window->iter),
		      COL_MAC_SRC, window->new_packet->mac_src,
		      COL_MAC_DST, window->new_packet->mac_dst,
		      COL_IP_SRC, window->new_packet->ip_src,
		      COL_IP_DST, window->new_packet->ip_dst,
		      COL_PROTOCOL, window->new_packet->protocol,
		      COL_PORT_SRC,window->new_packet->port_src,
		      COL_PORT_DST, window->new_packet->port_dst,
		      COL_APPLICATION, window->new_packet->application,
		      COL_DETAIL, window->new_packet->details,
		      -1);
  //free
  //free(window->new_packet);
  return GTK_TREE_MODEL (window->store);
}

GtkWidget *create_view_and_model (t_HomeWindow *window) {
  GtkCellRenderer     *renderer;
  GtkTreeSelection    *selection;
  GtkTreeViewColumn   *c;

  /* Create tree view */
  window->treeView = gtk_tree_view_new ();

  gtk_tree_view_set_headers_clickable(GTK_TREE_VIEW (window->treeView), TRUE);

  /* Set the click handler */
  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(window->treeView));
  gtk_tree_selection_set_select_function(selection, view_selection_func, window, NULL);

   /* --- Column #1 --- */
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Mac src",
                                               renderer,
                                               "text", COL_MAC_SRC,
                                               NULL);


  c = gtk_tree_view_get_column( GTK_TREE_VIEW (window->treeView), 0);
  gtk_tree_view_column_set_sort_column_id(c, 0);


  /* --- Column #2 --- */
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Mac dst",
                                               renderer,
                                               "text", COL_MAC_DST,
                                               NULL);

  c = gtk_tree_view_get_column( GTK_TREE_VIEW (window->treeView), 1);
  gtk_tree_view_column_set_sort_column_id(c, 1);


  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Ip src",
                                               renderer,
                                               "text", COL_IP_SRC,
                                               NULL);

  c = gtk_tree_view_get_column( GTK_TREE_VIEW (window->treeView), 2);
  gtk_tree_view_column_set_sort_column_id(c, 2);


  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Ip dst",
                                               renderer,
                                               "text", COL_IP_DST,
                                               NULL);

  c = gtk_tree_view_get_column( GTK_TREE_VIEW (window->treeView), 3);
  gtk_tree_view_column_set_sort_column_id(c, 3);


  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Protocol",
                                               renderer,
                                               "text", COL_PROTOCOL,
                                               NULL);
    c = gtk_tree_view_get_column( GTK_TREE_VIEW (window->treeView), 4);
  gtk_tree_view_column_set_sort_column_id(c, 4);

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Port src",
                                               renderer,
                                               "text", COL_PORT_SRC,
                                               NULL);

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "Port dst",
                                               renderer,
                                               "text", COL_PORT_DST,
                                               NULL);

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "application",
                                               renderer,
                                               "text", COL_APPLICATION,
                                               NULL);

  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeView),
                                               -1,
                                               "details",
                                               renderer,
                                               "text", COL_APPLICATION,
                                               NULL);

  /* fill the model with datas */
   window->store = gtk_list_store_new (NUM_COLS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

   //window->model = create_and_fill_model (window);
   //gtk_tree_view_set_model (GTK_TREE_VIEW (window->treeView), window->model);

  /* The tree view has acquired its own reference to the
   *  model, so we can drop ours. That way the model will
   *  be freed automatically when the tree view is destroyed */
  //g_object_unref (window->model);

  return window->treeView;
}
