#include "home_window.h"
#include "topbar.h"
#include "packet_list.h"
#include "packet_details.h"

void content (t_HomeWindow *window) {
  GtkWidget *view;
  GtkWidget *hbox = gtk_box_new(FALSE, 0);

  /* Create view and attach it to grid */
  view = create_view_and_model (window);

  gtk_container_add (GTK_CONTAINER(window->s_window), view); /* attache le treeview a la scrollbar*/

  /* add scrollview in container hbox*/
  gtk_container_add (GTK_CONTAINER (hbox), window->s_window);

  gtk_grid_attach (GTK_GRID (window->grid), hbox, 0, 2, 20, 1);
  gtk_widget_show_all (window->window);
}

void initScrollView(t_HomeWindow *window) {
  /* init scrollbar */
  window->s_window = gtk_scrolled_window_new (NULL, NULL);       /*init de la scrollbar*/
  gtk_widget_set_size_request(GTK_WIDGET(window->s_window), 700, 400);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(window->s_window), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS); /* copaing ! */
}

void initGrid (t_HomeWindow *window) {
   /* Here we construct the container that is going pack our content */
  window->grid = gtk_grid_new ();
  /* Pack the container in the window */
  gtk_container_add (GTK_CONTAINER (window->window), window->grid);
}

void initWindow (t_HomeWindow *window) {
  /* init window infos*/
  window->window = gtk_application_window_new (window->app);
  //  gtk_widget_set_size_request(GTK_WIDGET(window->window), 800, 800);
  gtk_window_set_title (GTK_WINDOW (window->window), "Window");
  gtk_window_set_resizable(GTK_WINDOW(window->window), FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (window->window), 10);

   /* init scrollable View */
  initScrollView(window);
  /* init grid */
  initGrid(window);
  /* init packet Details*/
  initPacketDetails(window);
}

void clearWindow(t_HomeWindow *window) {
   GList *children, *iter;

   /* Clear Children principal in window*/
  children = gtk_container_get_children(GTK_CONTAINER(window->window));
  for (iter = children; iter != NULL; iter = g_list_next(iter))
    gtk_widget_destroy(GTK_WIDGET(iter->data));
  g_list_free(children);

  /* Reset ScrollView and Grid */
  initScrollView(window);
  initGrid(window);
}
